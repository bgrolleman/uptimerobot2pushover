<?php
// Simple script to transform uptime robot requests to pushover
//
//
// Supply in get statement or hardcode
$APP_Token=$_REQUEST['api'];
$USER_Token=$_REQUEST['user'];

$monitorID=$_REQUEST['monitorID'];
$monitorURL=$_REQUEST['monitorURL'];
$monitorFriendlyName=$_REQUEST['monitorFriendlyName'];
$alertType=$_REQUEST['alertType'];
$alertDetails=$_REQUEST['alertDetails'];
$monitorAlertContacts=$_REQUEST['monitorAlertContacts'];
if ( $alertType == 1 ) {
  $alert = 'UP';
} else {
  $alert = 'DOWN';
};

?>
<html>
<body>
<p>Processing request</p>
<?
curl_setopt_array($ch = curl_init(), array(
	CURLOPT_URL => "https://api.pushover.net/1/messages.json",
	CURLOPT_POSTFIELDS => array(
		'token' => $APP_Token,
		'user' => $USER_Token,
		'title' => "$alert $monitorFriendlyName",
		'message' => $alertDetails,
		'url' => $monitorURL
	)));
curl_exec($ch);
curl_close($ch);
?>
<p>Send</p>
</body>
</html>
